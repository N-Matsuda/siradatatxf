﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;

namespace SiraDataTransmitter
{
    //SIRA転送処理実施ログを管理するクラス
    class LogData
    {
        private const string stAppName = "SiraDataTransmitter";

        //ログ書き込み
        public static void WriteLog(string Status)
        {
            //新しいレコードの追加
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                string sqlstr = "INSERT INTO LogData (プログラム,日付,メッセージ) VALUES ('" + stAppName + "','" + dt.ToString("yyyyMMddHHmm") + "',N'" + Status + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void DeleteOldLog()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            int hour = dt.Hour;
            if (hour < 23)  //23時以降に実施
                return;

            try
            {
                dt = dt.AddHours(-720); //テスト 30日前
                string sqlstr = "DELETE FROM LogData WHERE 日付 < '" + dt.ToString("yyMMddHHmm") + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }



    }
}
