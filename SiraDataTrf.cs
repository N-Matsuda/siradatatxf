﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using SiraDataTransmitter.ServiceReference1;
using System.Xml;
using System.Xml.Serialization;

namespace SiraDataTransmitter
{
    //SIRA用のデータをLOB転送用のフォーマットに変更し、LOBへ転送するためのクラス
    class SiraDataTrf
    {
        //Constructor
        private const int MAXTANKNO = 20;       //最大タンク数
        private string kurofilename;　//黒本データのファイル名
        //private string sircsvname;
        private string sirfilename;  //LOBに転送するファイル名
        private string cmpcode;      //会社コード
        private string sitecode;     //施設コード
        private string sitename;     //施設名
        private string skkcode;      //SKKコード
        //private bool hdsira;         //true == 高精度SIRAデータ
        SiraCompany sircmp;
        SiraSite sirsite;
        FilePath fp;
        int companynum;
        int sitenum;                //施設番号
        int seqnum;                 //施設連続番号
        const int RECOFFSET = 11;
        //通常SIRAデータ読み込みの定義
        const int NUMPERTANK = 42;  //タンクあたりバイト数
        const int NUMPERTANK2 = 32;  //タンクあたりバイト数
        const int INVOFFSET = 2;    //在庫オフセット
        const int DELIVOFFSET = 12; //荷卸し量オフセット
        const int SALESOFFSET = 22; //販売量オフセット
        //通常SIRAデータ（温度データ)読み込みの定義
        const int TMP_NUMPERTANK = 34; //タンクあたりのバイト数
        const int TMP_TNKNOOFFS = 0;   //タンク番号のオフセット
        const int TMP_TNKNOSIZE = 2;   //タンク番号のサイズ
        const int TMP_DLVSTTIMEOFFS = 2; //荷卸し開始時間オフセット (hhmm)
        const int TMP_DLVSTTIMESIZE = 4; //荷卸し開始時間サイズ
        const int TMP_DLVSTVOLOFFS = 6; //荷卸し開始時量オフセット
        const int TMP_DLVSTVOLSIZE = 6; //荷卸し開始時量サイズ
        const int TMP_DLVSTTMPOFFS = 12; //荷卸し開始時温度オフセット (XX.X℃)
        const int TMP_DLVSTTMPSIZE = 3;  //荷卸し開始時温度サイズ
        const int TMP_DLVEDTIMEOFFS = 15; //荷卸し終了時間オフセット (hhmm)
        const int TMP_DLVEDTIMESIZE = 4; //荷卸し終了時間サイズ
        const int TMP_DLVEDVOLOFFS = 19; //荷卸し終了時量オフセット
        const int TMP_DLVEDVOLSIZE = 6; //荷卸し終了時量サイズ
        const int TMP_DLVEDTMPOFFS = 25; //荷卸し終了時温度オフセット (XX.X℃)
        const int TMP_DLVEDTMPSIZE = 3;  //荷卸し終了時温度サイズ
        const int TMP_SALESVOLOFFS = 28; //販売量オフセット
        const int TMP_SALESVOLSIZE = 6;  //販売量サイズ

        //高精度SIRAデータ読み込みの定義
        //液面計データ
        const int NUMPERATGDT = 30;  //液面計データのサイズ
        const int ATGDATEOFFS = 1;   //時間のオフセット
        const int ATGDATESIZE = 10;  //時間データのサイズ
        const int ATGNUNTANK = 11;   //タンク数のオフセット
        const int ATGNNTNKSZ = 2;   //タンク数のオフセット
        const int ATGHDSZ = 13;     //液面計データヘッダーのサイズ
        const int ATGTNOOFFS = 0;   //タンク番号のオフセット
        const int ATGTNOSIZE = 2;    //タンク番号のサイズ
        const int ATGOILOFFS = 2;   //液種データのオフセット
        const int ATGOILSIZE = 2;    //液種データのサイズ
        const int ATGVOLOFFS = 4;   //液量のオフセット
        const int ATGVOLSIZE = 6;    //液量サイズ

        //販売データ
        const int NUMSALDT = 30;    //販売データのサイズ
        const int SALTNOOFFS = 1;   //タンク番号のオフセット
        const int SALTNOSIZE = 2;   //タンク番号のサイズ
        const int SALNZLOFFS = 3;   //ノズル番号のオフセット
        const int SALNZLSIZE = 1;   //ノズル番号のサイズ
        const int SALSTMOFFS = 4;   //販売開始時間のオフセット
        const int SALSTMSIZE = 10;  //販売開始時間のサイズ
        const int SALETMOFFS = 14;  //販売終了時間のオフセット
        const int SALETMSIZE = 10;  //販売終了時間のサイズ
        const int SALVOLOFFS = 24;  //販売量のオフセット
        const int SALVOLSIZE = 6;   //販売量のサイズ
        //荷卸しデータ
        const int NUMDLVDT = 30;    //荷卸しデータのサイズ
        const int DLVTNOOFFS = 1;   //タンク番号のオフセット
        const int DLVTNOSIZE = 2;   //タンク番号のサイズ
        const int DLVTMOFFS = 9;    //荷卸し時間のオフセット
        const int DLVTMSIZE = 10;   //荷卸し時間のサイズ
        const int DLVVOLOFFS = 3;  //荷卸し量のオフセット
        const int DLVVOLSIZE = 6;   //荷卸し量のサイズ

        //SIRAデータ転送実施
        public SiraDataTrf(string companycode, int cmpnum, int snum, int seqn)
        {
            //kurofilename = fname;
            sirsite = new SiraSite();
            sirsite.OpenTable(companycode);
            sircmp = new SiraCompany();
            sircmp.OpenTable("*");
            companynum = cmpnum;
            sitenum = snum;
            cmpcode = sircmp.GetCompanyCode(companynum); //会社コード
            sitecode = sirsite.GetSiteCode(sitenum); //施設コード
            sitename = sirsite.GetSiteName(sitenum);
            skkcode = sirsite.GetSKKCode(sitenum);
            //hdsira = sirsite.GetHDSira(sitenum);

            fp = new FilePath(sircmp.GetFolderName(companynum));
            //initmth = DateTime.Now;
            //endmth = DateTime.Now;
            seqnum = seqn;
        }

        //SIRファイルの解析
        public bool SirDataAnalysis(string datastr)
        {
            return AnalyzeDailySIRA(datastr);
        }
        //SIRAファイル(温度データ)の解析
        public bool SirTempDataAnalysis()
        {
            //return AnalyzeDailyTempSIRA();
            return false;
        }

        //高精度SIRの解析
        public bool HDSirDataAnalysis()
        {
            //return AnalyzeHdSIRA();
            return false;
        }

        //通常SIRAデータの分析とCSVファイルの作成
        private bool AnalyzeDailySIRA(string kurodatastr)
        {
            bool bret = true;
            //KuroCsvData[] kcsvdat = new KuroCsvData[MAXTANKNO];
            DateTime dt = DateTime.Now.ToLocalTime();
            //DateTime lastday = DateTime.Now;
            string kurodatestr = "";
            try
            {
                string csvstr = "";
                string datestr;
                string tnumstr;
                string relchar; //データ信頼性
                int tanknum;
                int salesnum, delivnum, invnum, calcinv, invdif;
                int iComp;
                int recordnum = 1;
                int numpertnk = NUMPERTANK;

                var tnolist = new List<string>();   //タンク番号リスト
                var invlist = new List<string>();   //在庫量リスト
                var saleslist = new List<string>(); //販売量リスト
                var delivlist = new List<string>();　//荷下ろし量リスト

                datestr = kurodatastr.Substring(0, 8);  //日付文字列取り出し
                kurodatestr = kurodatastr.Substring(8);

                string chkdatestr = datestr.Insert(6, "-").Insert(4, "-");
                dt = DateTime.Parse(chkdatestr.Replace("-", "/"));
                string savedatestr = datestr;
                datestr = datestr.Insert(6, "-").Insert(4, "-");
                relchar = kurodatastr.Substring(8, 1);
                if ((relchar == "a") || (relchar == "@") || (relchar == "A"))
                     numpertnk = NUMPERTANK2;
                tnumstr = kurodatastr.Substring(9, 2);　//タンク数文字列取り出し
                tanknum = int.Parse(tnumstr);           //タンク数取り出し
                Console.WriteLine("start analyze " + skkcode);
                if ((tanknum > 0) && (tanknum <= 20))
                {
                    for (int i = 0; i < tanknum; i++)
                    {
                        if ((true == sirsite.GetTankNotLinkedOrLinkedFirst(sitenum, i + 1)) && (0 != sirsite.GetTankCapa(sitenum, i + 1))) //連結でないか、連結タンクの先頭ならばレコード作成
                        {
                            //会社コード、施設コード、液種、タンクID書き込み
                            string oltype = sirsite.GetTankOilType(sitenum, i + 1);
                            if (oltype.StartsWith("Waste") == true)
                                continue;

                            string tno;
                            tno = (i + 1).ToString();
                            tnolist.Add(tno);

                            csvstr = csvstr + "\"" + cmpcode + "\",\"" + sitecode + "\",\""; //会社コード、施設コード
                            csvstr = csvstr + oltype + "\",\""; //液種
                            csvstr = csvstr + "REC" + seqnum.ToString() + recordnum.ToString() + (i + 1).ToString() + "\",\""; //レコードID
                            csvstr = csvstr + sirsite.GetTankString(sitenum, i + 1) + "\",\""; //タンクID
                            csvstr = csvstr + datestr + "\",\""; //日付；

                            //販売量取得,書き込み
                            salesnum = int.Parse(kurodatastr.Substring(RECOFFSET + numpertnk * i + SALESOFFSET, 7));
                            if ((sitecode == "JSS061") && (tno == "2"))
                                salesnum *= 10;
                            iComp = kurodatastr.Substring(RECOFFSET + numpertnk * i + SALESOFFSET + 8, 1).CompareTo("5"); //販売量四者五入
                            if (iComp >= 0)
                                salesnum++;
                            csvstr = csvstr + salesnum.ToString() + "\",\""; //販売量
                            saleslist.Add(salesnum.ToString());

                            //荷下ろし量、在庫量取得
                            delivnum = int.Parse(kurodatastr.Substring(RECOFFSET + numpertnk * i + DELIVOFFSET, 7));
                            invnum = int.Parse(kurodatastr.Substring(RECOFFSET + numpertnk * i + INVOFFSET, 7));
                            int lastinv = sirsite.GetTankLMInv(sitenum, i + 1);
                            sirsite.SetTankLMInv(sitenum, i + 1, invnum);　//SIRA移設情報の最新の在庫量書き込み
                            calcinv = lastinv + delivnum - salesnum;

                            //計算在庫と実在庫の差
                            if (calcinv > invnum)
                            {
                                //invdif = calcinv - invnum + 300;
                                invdif = calcinv - invnum;
                            }
                            else //calcinv <= invnum
                            {
                                //invdif = invnum - calcinv + 300;
                                invdif = invnum - calcinv;
                            }

                            //計算在庫と実在庫の差が大きい場合は荷卸し量を調整する
                            if (invdif > 500)
                            {
                                invdif = ((invdif + 500) / 1000) * 1000;
                                if (calcinv > invnum)
                                {
                                    if (delivnum >= invdif)
                                        delivnum = delivnum - invdif;
                                }
                                else
                                {
                                    delivnum += invdif;
                                }
                            }
                            csvstr = csvstr + delivnum.ToString() + "\",\""; //荷卸し量
                            csvstr = csvstr + invnum.ToString() + "\"\r"; //在庫量
                            delivlist.Add(delivnum.ToString());
                            invlist.Add(invnum.ToString());
                            //Console.WriteLine("Write tank " + tno.ToString());
                        }
                        recordnum++;
                        //kurodatastr = kurodatastr.Substring(RECOFFSET + numpertnk * tanknum);
                    }
                }

                //ログの作成
                DateTime dtn = DateTime.Now.ToLocalTime();
                //転送用のcsvファイルを保存する
                sirfilename = fp.GetSirCSVFilePath(cmpcode, sitecode, dtn);
                Console.WriteLine("Write filename " + sirfilename);
                StreamWriter writer = new StreamWriter(sirfilename, true, Encoding.GetEncoding(20127));
                writer.Write(csvstr);
                writer.Close();
                Console.WriteLine("write daily sira data " + sirfilename);

                sirsite.UpdateDBLMInv();        //SIRA施設のDB更新
                                                //SIRA履歴のDB保存
                SiraHistory.SaveSiraHistory(skkcode, savedatestr, tnolist, invlist, saleslist, delivlist);

            }
            catch (Exception ex)
            {
#if false
                try
                {
                    for (int i = 0; i < MAXTANKNO; i++)            //黒本PDFの元になるCSVファイルを保存する。
                    {
                        kcsvdat[i].CloseKuroFile((i + 1).ToString(), dt);
                    }
                }
                catch (Exception ex2)
                {
                    ;
                }
#endif
                Console.WriteLine(sitename + "failed to create sira file");
                bret = false;
            }
            return bret;
        }


        //分析開始月と終了月が異なるかとうかチェック
        public bool CheckStartEndMonth()
        {
            bool bret = false;
            //if (initmth.Month != endmth.Month)
            //    bret = true;
            return bret;
        }

        //SIRA DailyデータをLOBサーバーへSOAPにて転送
        //SOAPのサービス定義はVisualStudioのプロジェクトメニュー⇒サービス参照の追加により、
        //LOB社より提供されたWSDLのアドレスを入力することにより作成
        public bool SiraDataTrfExec(bool hdsira)
        {
            bool bRet = false;
            var inputFileName = sirfilename;
            string sendfilename = Path.GetFileName(sirfilename);
            byte[] fileBytes = File.ReadAllBytes(inputFileName);
            var client = new DataServiceClient("BasicHttpBinding_IDataService");
            client.ClientCredentials.UserName.UserName = sircmp.GetUserName(companynum);
            client.ClientCredentials.UserName.Password = sircmp.GetPassword(companynum);

            var httpRequestProperty = new HttpRequestMessageProperty();

            httpRequestProperty.Headers[HttpRequestHeader.Authorization] = "Basic " +
                Convert.ToBase64String(Encoding.UTF8.GetBytes(client.ClientCredentials.UserName.UserName + ":" + client.ClientCredentials.UserName.Password));

            using (var scope = new OperationContextScope(client.InnerChannel))
            {
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                try
                {
                    DateTime dt = DateTime.Now.ToLocalTime();
                    if (hdsira == true) //高精度SIRAデータの転送
                    {
                        var response = client.SubmitHdSiraData(new DataServiceDataRequest
                        {
                            DataServiceDto = new DataServiceDTO
                            {
                                CompanyCode = cmpcode,
                                FileName = sendfilename,
                                FileType = FileType.Xml,
                                TimeStamp = DateTime.Now.ToLocalTime(),
                                Payload = fileBytes,
                            }
                        });
                        if (response.ResponseMessage.ToString() == "Data submitted successfully.")
                        {
                            string rtext = response.ExtensionData.ToString();
                            Console.WriteLine(rtext);
                            LogData.WriteLog(sendfilename + " HDSIRAの転送成功");
                            bRet = true;
                        }
                        else
                        {
                            LogData.WriteLog(sendfilename + " HDSIRAの転送失敗");
                            bRet = false;
                        }
                    }
                    else　　　　　　//通常SIRAデータの転送
                    {
                        var response = client.SubmitSIRAData(new DataServiceDataRequest
                        {
                            DataServiceDto = new DataServiceDTO
                            {
                                CompanyCode = cmpcode,
                                FileName = sendfilename,
                                //FileName = @"D:\SIRA_JS0003_20150204.csv",
                                FileType = FileType.SIRADataInCSV,
                                TimeStamp = DateTime.Now.ToLocalTime(),
                                Payload = fileBytes,
                            }
                        });
                        if (response.ResponseMessage.ToString() == "Data submitted successfully.")
                        {
                            string rtext = response.ExtensionData.ToString();
                            Console.WriteLine(rtext);
                            //メール転送
                            //string mailadr = sircmp.GetMailAddress(companynum); //メールアドレス
                            //if (mailadr != "")
                            //{
                            //    SendSIRMail(mailadr);
                            //}
                            Console.WriteLine("Data submission succeed filename " + sendfilename );
                            LogData.WriteLog(sendfilename + " SIRAの転送成功");
                            bRet = true;
                        }
                        else
                        {
                            Console.WriteLine("Data submission failed");
                            LogData.WriteLog(sendfilename + " SIRAの転送失敗");
                            bRet = false;
                        }
                    }

                }
                catch (Exception ex)
                {
                    if (hdsira == true)
                        LogData.WriteLog(sitename + " HDSIRAの転送失敗");
                    else
                        LogData.WriteLog(sitename + " SIRAの転送失敗");
                    Console.WriteLine(ex.ToString());
                    bRet = false;
                }
            }
            try
            {
                File.Delete(sirfilename);
            }
            catch (Exception ex)
            {
                if (hdsira == true)
                    LogData.WriteLog(sitename + " HDSIRAファイルの削除失敗");
                else
                    LogData.WriteLog(sitename + " SIRAファイルの削除失敗");
                bRet = false;
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Failed to delete csv file");
            }
            return bRet;
        }


#if false

        //ユーザー向けメールの送信
        private void SendSIRMail(string mladr)
        {
            StreamReader sr = new StreamReader(kurofilename, Encoding.GetEncoding(20127)); //ASCII
            string sndmsg = sr.ReadToEnd();
            int digiidx = sndmsg.IndexOf("<idigi_data>");
            sndmsg = sndmsg.Substring(digiidx + 12); //12 - <idigi_data>"
            sr.Close();
            string pdfpath = fp.GetTodayPdfDataPath(cmpcode, sitecode);

            if (sndmsg != "")
            {
                try
                {
                    DateTime dt = DateTime.Now.ToLocalTime();
                    sndmsg = "SIRAデータ転送履歴 :" + dt.ToString("yyyy年MM月dd日 HH時mm分") + "\r\n" + sndmsg;
                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage("n-matsuda@skk-atgs.jp", mladr, "SIRデータ転送履歴　施設:" + sirsite.GetSiteName(sitenum), sndmsg);
                    System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
                    //SMTPサーバーなどを設定する
                    sc.Host = "mail.skk-atgs.jp";
                    sc.Port = 25;
                    sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    //ユーザー名とパスワードを設定する
                    sc.Credentials = new System.Net.NetworkCredential("n-matsuda@skk-atgs.jp", "risudon");
                    if (System.IO.File.Exists(pdfpath) == true)
                    {
                        System.Net.Mail.Attachment attfile = new System.Net.Mail.Attachment(pdfpath);
                        attfile.ContentType = new System.Net.Mime.ContentType("application/pdf");
                        msg.Attachments.Add(attfile);
                    }

                    //メッセージを送信する
                    sc.Send(msg);

                    //後始末
                    msg.Dispose();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }


        //高精度　SIRAデータの分析とXMLファイルの作成
        //XMLファイルの定義はLOB_HD_SIRA_Schema.xsdよりLOB_HD_SIRA_Chema.csでクラス定義されている。
        //csファイルはVisual Studioのxsdツールにより作成
        private bool AnalyzeHdSIRA()
        {
            bool bret = true;
            try
            {
                HDSiraSite hdsiradat = new HDSiraSite();
                hdsiradat.OpenTable(skkcode);
                string curatgdate = hdsiradat.GetCurATGDate(); //液面計最新データの日付
                string cursalesdate = hdsiradat.GetCurSalesDate(); //販売最新データの日付
                string curdelivdate = hdsiradat.GetCurDelivDate(); //荷卸し最新データの日付

                LogData.WriteLog(sitename + " HDSIRAの解析開始");

                //レギュラー
                LOB_HD_SIRACompanySiteGrade sirunlead = new LOB_HD_SIRACompanySiteGrade();
                sirunlead.ProductName = "Unlead";
                bool bunleadex = false;
                //レギュラー液面計記録のリスト、後で配列に変換しsirunleadに追加する。
                List<LOB_HD_SIRACompanySiteGradeATGRecord> unleadatgarr = new List<LOB_HD_SIRACompanySiteGradeATGRecord>();
                //レギュラー荷卸し記録のリスト、後で配列に変換しsirunleadに追加する。
                List<LOB_HD_SIRACompanySiteGradeDeliveryRecord> unleaddlvarr = new List<LOB_HD_SIRACompanySiteGradeDeliveryRecord>();
                //レギュラー販売記録のリスト、後で配列に変換しsirunleadに追加する。
                List<LOB_HD_SIRACompanySiteGradeSaleRecord> unleadsalearr = new List<LOB_HD_SIRACompanySiteGradeSaleRecord>();

                //プレミアム
                LOB_HD_SIRACompanySiteGrade sirpremium = new LOB_HD_SIRACompanySiteGrade();
                sirpremium.ProductName = "Premium";
                bool bpremiumex = false;
                //プレミアム液面計記録のリスト、後で配列に変換しsirpremiumに追加する。
                List<LOB_HD_SIRACompanySiteGradeATGRecord> pulpatgarr = new List<LOB_HD_SIRACompanySiteGradeATGRecord>();
                //プレミアム荷卸し記録のリスト、後で配列に変換しsirpremmiumに追加する。
                List<LOB_HD_SIRACompanySiteGradeDeliveryRecord> pulpdlvarr = new List<LOB_HD_SIRACompanySiteGradeDeliveryRecord>();
                //プレミアム販売記録のリスト、後で配列に変換しsirpremiumに追加する。
                List<LOB_HD_SIRACompanySiteGradeSaleRecord> pulpsalearr = new List<LOB_HD_SIRACompanySiteGradeSaleRecord>();


                //軽油
                LOB_HD_SIRACompanySiteGrade sirdiesel = new LOB_HD_SIRACompanySiteGrade();
                sirdiesel.ProductName = "Diesel";
                bool bdieselex = false;
                //軽油液面計記録のリスト、後で配列に変換しsirdieselに追加する。
                List<LOB_HD_SIRACompanySiteGradeATGRecord> dslatgarr = new List<LOB_HD_SIRACompanySiteGradeATGRecord>();
                //軽油荷卸し記録のリスト、後で配列に変換しsirpdieselに追加する。
                List<LOB_HD_SIRACompanySiteGradeDeliveryRecord> dsldlvarr = new List<LOB_HD_SIRACompanySiteGradeDeliveryRecord>();
                //軽油販売記録のリスト、後で配列に変換しsirdieselに追加する。
                List<LOB_HD_SIRACompanySiteGradeSaleRecord> dslsalearr = new List<LOB_HD_SIRACompanySiteGradeSaleRecord>();

                //灯油
                LOB_HD_SIRACompanySiteGrade sirkerosene = new LOB_HD_SIRACompanySiteGrade();
                sirkerosene.ProductName = "Kerosine";
                bool bkeroseneex = false;
                //灯油液面計記録のリスト、後で配列に変換しsirkeroseneに追加する。
                List<LOB_HD_SIRACompanySiteGradeATGRecord> krsatgarr = new List<LOB_HD_SIRACompanySiteGradeATGRecord>();
                //灯油荷卸し記録のリスト、後で配列に変換しsirkeroseneに追加する。
                List<LOB_HD_SIRACompanySiteGradeDeliveryRecord> krsdlvarr = new List<LOB_HD_SIRACompanySiteGradeDeliveryRecord>();
                //灯油販売記録のリスト、後で配列に変換しsirkeroseneに追加する。
                List<LOB_HD_SIRACompanySiteGradeSaleRecord> krssalearr = new List<LOB_HD_SIRACompanySiteGradeSaleRecord>();


                for (int i = 0; i < MAXTANKNO; i++)
                {
                    if (true == sirsite.GetTankNotLinkedOrLinkedFirst(sitenum, i + 1)) //連結でないか、連結タンクの先頭ならばレコード作成
                    {
                        switch (sirsite.GetTankOilType(sitenum, i + 1))
                        {
                            default:
                            case "Unlead":
                                bunleadex = true;
                                break;
                            case "PULP":
                                bpremiumex = true;
                                break;
                            case "Diesel":
                                bdieselex = true;
                                break;
                            case "Kerosene":
                                bkeroseneex = true;
                                break;
                        }
                    }
                }


                //ここで実際のデータを解析して、配送、販売、在庫データを対応する液種に挿入する。
                string sirdatastr;
                string mrkstr;
                string recstr;          //配送、販売、在庫各レコード文字列
                string itemstr;         //各レコード内項目文字列
                string oiltype;
                decimal decval;
                string filedatestr;
                int recordnum = 1;
                int tno, numtank;
                DateTime curdt;

                StreamReader sr = new StreamReader(kurofilename, Encoding.GetEncoding(20127)); //ASCII
                filedatestr = System.IO.Path.GetFileName(kurofilename);
                filedatestr = filedatestr.Substring(12);
                filedatestr = filedatestr.Insert(8, "-");
                sirdatastr = sr.ReadToEnd();
                int digiidx = sirdatastr.IndexOf("<idigi_data>");
                sirdatastr = sirdatastr.Substring(digiidx + 12); //12 - <idigi_data>"
                sr.Close();

                while (true)
                {
                    try
                    {
                        mrkstr = sirdatastr.Substring(0, 1);  //データ種類取り出し
                        if (mrkstr == "<")              //最終まで読み込んだか？
                            break;
                        switch (mrkstr)
                        {
                            default:
                            case "A":                   //液面計データ
                                //recstr = sirdatastr.Substring(0,NUMPERATGDT); //液面計記録取り出し
                                //sirdatastr = sirdatastr.Substring(NUMPERATGDT);
                                //itemstr = recstr.Substring(ATGDATEOFFS, ATGDATESIZE) + "00"; //時間取り出し
                                itemstr = "20" + sirdatastr.Substring(ATGDATEOFFS, ATGDATESIZE);
                                if (curatgdate.CompareTo(itemstr) > 0) //登録済のデータより新しいかチェックする
                                {
                                    itemstr = sirdatastr.Substring(ATGNUNTANK, ATGNNTNKSZ); //タンク数
                                    numtank = int.Parse(itemstr);
                                    sirdatastr = sirdatastr.Substring(ATGHDSZ + NUMPERATGDT * numtank);
                                    break;
                                }
                                curdt = DateTime.ParseExact(curatgdate + "00", "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                itemstr = itemstr + "00"; //時間取り出し
                                dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                TimeSpan ts = dt - curdt;
                                if (ts.Hours >= 1)  //1時間以上の未登録時間がある。
                                {
                                    logdat.OpenLogfile();
                                    logdat.WriteLog(dt, sitename, Path.GetFileName(kurofilename) + ts.Hours.ToString() + "時間の液面計データ未登録時間あり");
                                    logdat.CloseLogTable();
                                }
                                curatgdate = itemstr.Substring(0, 12);
                                itemstr = sirdatastr.Substring(ATGNUNTANK, ATGNNTNKSZ); //タンク数
                                numtank = int.Parse(itemstr);
                                for (int i = 0; i < numtank; i++)
                                {
                                    LOB_HD_SIRACompanySiteGradeATGRecord atgrec = new LOB_HD_SIRACompanySiteGradeATGRecord();
                                    recstr = sirdatastr.Substring(ATGHDSZ + NUMPERATGDT * i, NUMPERATGDT);
                                    itemstr = recstr.Substring(ATGTNOOFFS * 1, ATGTNOSIZE); //タンク番号文字列取り出し
                                    tno = int.Parse(itemstr);
                                    atgrec.ATGRecordID = "ATG" + recordnum.ToString() + (i + 1).ToString();  //液面計レコードID
                                    atgrec.TankID = "Tank" + tno.ToString();
                                    atgrec.ATGRecordDateTime = dt;
                                    oiltype = sirsite.GetTankOilType(sitenum, tno);
                                    itemstr = recstr.Substring(ATGVOLOFFS, ATGVOLSIZE);    //液量
                                    decval = decimal.Parse(itemstr);
                                    atgrec.ProductVolumeCurrent = decval;

                                    switch (sirsite.GetTankOilType(sitenum, tno))
                                    {
                                        default:
                                        case "Unlead":
                                            unleadatgarr.Add(atgrec);
                                            break;
                                        case "PULP":
                                            pulpatgarr.Add(atgrec);
                                            break;
                                        case "Diesel":
                                            dslatgarr.Add(atgrec);
                                            break;
                                        case "Kerosene":
                                            krsatgarr.Add(atgrec);
                                            break;
                                    }

                                }
                                sirdatastr = sirdatastr.Substring(ATGHDSZ + NUMPERATGDT * numtank);

#if false
                            itemstr = recstr.Substring(ATGOLVLOFFS,ATGOLVLSIZE);   //液位
                            itemstr = itemstr.Insert(ATGOLVLSIZE-1, ".");
                            decval = decimal.Parse(itemstr);
                            atgrec.ProductLevelCurrentSpecified = true;
                            atgrec.ProductLevelCurrent = decval;

                            itemstr = recstr.Substring(ATGWLVLOFFS, ATGWLVLSIZE);  //水位
                            itemstr = itemstr.Insert(ATGWLVLSIZE-1, ".");
                            decval = decimal.Parse(itemstr);
                            atgrec.WaterLevelCurrent = decval;

                            itemstr = recstr.Substring(ATGTEMPOFFS, ATGTEMPSIZE);  //温度
                            itemstr = itemstr.Insert(ATGTEMPSIZE-1, ".");
                            decval = decimal.Parse(itemstr);

                            atgrec.ProductTemperatureCurrent = decval;
                            atgrec.TankID = "Tank" + tno.ToString();            //タンクID
                            atgrec.ATGRecordID = "ATG" + recordnum.ToString();  //液面計レコードID
                            oiltype = sirsite.GetTankOilType(sitenum, tno);
                            switch (sirsite.GetTankOilType(sitenum, tno))
                            {
                                default:
                                case "Unlead":
                                    unleadatgarr.Add(atgrec);
                                    break;
                                case "PULP":
                                    pulpatgarr.Add(atgrec);
                                    break;
                                case "Diesel":
                                    dslatgarr.Add(atgrec);
                                    break;
                                case "Kerosene":
                                    krsatgarr.Add(atgrec);
                                    break;
                            }
#endif
                                break;
                            case "D":                   //荷卸しデータ
                                recstr = sirdatastr.Substring(0, NUMDLVDT);
                                sirdatastr = sirdatastr.Substring(NUMDLVDT);               //荷卸しデータ取り出し
                                LOB_HD_SIRACompanySiteGradeDeliveryRecord delvrec = new LOB_HD_SIRACompanySiteGradeDeliveryRecord();
                                //itemstr = recstr.Substring(DLVTMOFFS, DLVTMSIZE) + "00"; //時間取り出し
                                itemstr = "20" + recstr.Substring(DLVTMOFFS, DLVTMSIZE);
                                if (curdelivdate.CompareTo(itemstr) > 0) //登録済のデータより新しいかチェックする
                                {
                                    break;
                                }
                                curdelivdate = itemstr;
                                itemstr = itemstr + "00"; //時間取り出し
                                dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                delvrec.DeliveryDateTime = dt;

                                itemstr = recstr.Substring(DLVVOLOFFS, DLVVOLSIZE);     //荷卸し量
                                decval = decimal.Parse(itemstr);
                                delvrec.DeliveryVolumeMetered = decval;

                                itemstr = recstr.Substring(SALTNOOFFS, SALTNOSIZE); //タンク番号文字列取り出し
                                tno = int.Parse(itemstr);
                                delvrec.TankID = "Tank" + tno.ToString();                   //タンクID
                                delvrec.DeliveryRecordID = "DELV" + recordnum.ToString();    //荷卸レコードID
                                oiltype = sirsite.GetTankOilType(sitenum, tno);
                                switch (sirsite.GetTankOilType(sitenum, tno))
                                {
                                    default:
                                    case "Unlead":
                                        unleaddlvarr.Add(delvrec);
                                        break;
                                    case "PULP":
                                        pulpdlvarr.Add(delvrec);
                                        break;
                                    case "Diesel":
                                        dsldlvarr.Add(delvrec);
                                        break;
                                    case "Kerosene":
                                        krsdlvarr.Add(delvrec);
                                        break;
                                }
                                break;
                            case "S":                   //販売データ
                                recstr = sirdatastr.Substring(0, NUMSALDT);    //販売データ取り出し
                                sirdatastr = sirdatastr.Substring(NUMSALDT);
                                LOB_HD_SIRACompanySiteGradeSaleRecord salrec = new LOB_HD_SIRACompanySiteGradeSaleRecord();

                                itemstr = recstr.Substring(SALNZLOFFS, SALNZLSIZE); //ノズル番号
                                salrec.NozzleID = itemstr;

                                //itemstr = recstr.Substring(SALSTMOFFS, SALSTMSIZE) + "00"; //開始時間取り出し
                                itemstr = "20" + recstr.Substring(SALSTMOFFS, SALSTMSIZE) + "00"; //開始時間取り出し
                                dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                salrec.SaleDateTimeStart = dt;

                                //itemstr = recstr.Substring(SALETMOFFS, SALETMSIZE) + "00"; //終了時間取り出し
                                itemstr = "20" + recstr.Substring(SALETMOFFS, SALETMSIZE); //終了時間取り出し
                                if (cursalesdate.CompareTo(itemstr) > 0) //登録済のデータより新しいかチェックする
                                {
                                    break;
                                }
                                curdt = DateTime.ParseExact(cursalesdate + "00", "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                itemstr = itemstr + "00";
                                dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                ts = dt - curdt;
                                if (ts.Hours >= 24) //1日以上の未登録時間がある
                                {
                                    logdat.OpenLogfile();
                                    logdat.WriteLog(dt, sitename, Path.GetFileName(kurofilename) + ts.Hours.ToString() + "時間の販売データ未登録時間あり");
                                    logdat.CloseLogTable();
                                }
                                cursalesdate = itemstr.Substring(0, 12);

                                salrec.SaleDateTimeEnd = dt;
                                itemstr = recstr.Substring(SALVOLOFFS, SALVOLSIZE); //販売量
                                itemstr = itemstr.Insert(SALVOLSIZE - 2, ".");
                                decval = decimal.Parse(itemstr);
                                salrec.SaleVolume = decval;

                                itemstr = recstr.Substring(DLVTNOOFFS, DLVTNOSIZE); //タンク番号取り出し
                                tno = int.Parse(itemstr);
                                salrec.TankID = "Tank" + tno.ToString();            //タンクID
                                salrec.SaleRecordID = "SALE" + recordnum.ToString();  //液面計レコードID

                                oiltype = sirsite.GetTankOilType(sitenum, tno);

                                switch (sirsite.GetTankOilType(sitenum, tno))
                                {
                                    default:
                                    case "Unlead":
                                        unleadsalearr.Add(salrec);
                                        break;
                                    case "PULP":
                                        pulpsalearr.Add(salrec);
                                        break;
                                    case "Diesel":
                                        dslsalearr.Add(salrec);
                                        break;
                                    case "Kerosene":
                                        krssalearr.Add(salrec);
                                        break;
                                }
                                break;
                        }
                        recordnum++;
                    }
                    catch (Exception ex)
                    {
                        LogData.WriteLog(sitename + Path.GetFileName(kurofilename) + " HDSiraデータの解析失敗1");
                        Console.WriteLine(ex.ToString());
                        break;
                    }
                }
                LOB_HD_SIRACompanySiteGradeATGRecord[] unleadatgid = unleadatgarr.ToArray();
                if (unleadatgid.GetLength(0) > 0)
                    sirunlead.ATGRecord = unleadatgid;
                LOB_HD_SIRACompanySiteGradeATGRecord[] pulpatgid = pulpatgarr.ToArray();
                if (pulpatgid.GetLength(0) > 0)
                    sirpremium.ATGRecord = pulpatgid;
                LOB_HD_SIRACompanySiteGradeATGRecord[] dslatgid = dslatgarr.ToArray();
                if (dslatgid.GetLength(0) > 0)
                    sirdiesel.ATGRecord = dslatgid;
                LOB_HD_SIRACompanySiteGradeATGRecord[] krsatgid = krsatgarr.ToArray();
                if (krsatgid.GetLength(0) > 0)
                    sirkerosene.ATGRecord = krsatgid;
                LOB_HD_SIRACompanySiteGradeDeliveryRecord[] unleaddlvid = unleaddlvarr.ToArray();
                if (unleaddlvid.GetLength(0) > 0)
                    sirunlead.DeliveryRecord = unleaddlvid;
                LOB_HD_SIRACompanySiteGradeDeliveryRecord[] pulpdlvid = pulpdlvarr.ToArray();
                if (pulpdlvid.GetLength(0) > 0)
                    sirpremium.DeliveryRecord = pulpdlvid;
                LOB_HD_SIRACompanySiteGradeDeliveryRecord[] dsldlvid = dsldlvarr.ToArray();
                if (dsldlvid.GetLength(0) > 0)
                    sirdiesel.DeliveryRecord = dsldlvid;
                LOB_HD_SIRACompanySiteGradeDeliveryRecord[] krsdlvid = krsdlvarr.ToArray();
                if (krsdlvid.GetLength(0) > 0)
                    sirkerosene.DeliveryRecord = krsdlvid;
                LOB_HD_SIRACompanySiteGradeSaleRecord[] unleadsaleid = unleadsalearr.ToArray();
                if (unleadsaleid.GetLength(0) > 0)
                    sirunlead.SaleRecord = unleadsaleid;
                LOB_HD_SIRACompanySiteGradeSaleRecord[] pulpsaleid = pulpsalearr.ToArray();
                if (pulpsaleid.GetLength(0) > 0)
                    sirpremium.SaleRecord = pulpsaleid;
                LOB_HD_SIRACompanySiteGradeSaleRecord[] dslsaleid = dslsalearr.ToArray();
                if (dslsaleid.GetLength(0) > 0)
                    sirdiesel.SaleRecord = dslsaleid;
                LOB_HD_SIRACompanySiteGradeSaleRecord[] krssaleid = krssalearr.ToArray();
                if (krssaleid.GetLength(0) > 0)
                    sirkerosene.SaleRecord = krssaleid;

                LOB_HD_SIRACompanySite sircmpsite = new LOB_HD_SIRACompanySite();
                sircmpsite.SiteCode = sitecode;
                List<LOB_HD_SIRACompanySiteGrade> gradearr = new List<LOB_HD_SIRACompanySiteGrade>();
                if (bunleadex == true)
                    gradearr.Add(sirunlead);
                if (bpremiumex == true)
                    gradearr.Add(sirpremium);
                if (bdieselex == true)
                    gradearr.Add(sirdiesel);
                if (bkeroseneex == true)
                    gradearr.Add(sirkerosene);
                LOB_HD_SIRACompanySiteGrade[] gradeid = gradearr.ToArray();
                sircmpsite.Grade = gradeid;

                LOB_HD_SIRACompanySite[] siteid = { sircmpsite }; //施設IDリスト

                LOB_HD_SIRACompany siracmp = new LOB_HD_SIRACompany(); //会社クラス
                siracmp.CompanyCode = cmpcode;　　//会社コード
                siracmp.Site = siteid;          　//施設IDリスト

                LOB_HD_SIRA hdsiradt = new LOB_HD_SIRA(); //HD SIRAクラス
                dt = DateTime.Now.ToLocalTime();
                hdsiradt.RecordDateTime = dt; //タイムスタンプ
                hdsiradt.Company = siracmp;  //会社クラス

                //作成したXMLファイルをファイル名をつけてシリアライズする。
                XmlSerializer serializer = new XmlSerializer(hdsiradt.GetType());
                sirfilename = fp.GetXMLDataPath(cmpcode, filedatestr);
                //XMLファイルに書き込み
                FileStream fs = new FileStream(sirfilename, FileMode.Create);

                hdsiradat.SetCurATGDate(curatgdate); //液面計最新データ日付のセット
                hdsiradat.SetCurSalesDate(cursalesdate); //販売最新データ日付のセット
                hdsiradat.SetCurDelivDate(curdelivdate); //荷卸し最新データの日付のセット
                hdsiradat.SetCurDateDB(); //最新データ日付時間のDB登録
                try
                {
                    serializer.Serialize(fs, hdsiradt);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    fs.Close();
                }


            }
            catch (Exception ex)
            {
                LogData.WriteLog(sitename + Path.GetFileName(kurofilename) + " HDSiraデータの解析失敗2");
                Console.WriteLine(ex.ToString());
                bret = false;
            }
            return bret;

        }

        //SIRA月結果取得
        public bool SiraGetMonthlyResult()
        {
            bool bRet = true;
            var client = new DataServiceClient("BasicHttpBinding_IDataService");
            client.ClientCredentials.UserName.UserName = sircmp.GetUserName(companynum);
            client.ClientCredentials.UserName.Password = sircmp.GetPassword(companynum);

            DateTime dt = DateTime.Now.ToLocalTime();
            dt = dt.AddMonths(-1);
            var httpRequestProperty = new HttpRequestMessageProperty();

            httpRequestProperty.Headers[HttpRequestHeader.Authorization] = "Basic " +
                Convert.ToBase64String(Encoding.UTF8.GetBytes(client.ClientCredentials.UserName.UserName + ":" + client.ClientCredentials.UserName.Password));

            using (var scope = new OperationContextScope(client.InnerChannel))
            {
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                try
                {
                    var response = client.GetSIRAAnalysisResults(new SIRAAnalysisDataRequest
                    {
                        SIRAAnalysisDataRequestDto = new SIRAAnalysisDataRequestDTO
                        {
                            CompanyCode = cmpcode,
                            AnalysisMonth = dt,
                            ResultType = null,
                        }
                    });
                    if (response.ResponseMessage.ToString() == "Data submitted successfully.")
                    {
                        foreach (var data in response.SIRAAnalysisResultData)
                        {
                            Console.WriteLine(data.ToString());
                        }
                        string rtext = response.ExtensionData.ToString();
                        Console.WriteLine(rtext);
                        LogData logdat = new LogData(sircmp.GetFolderName(companynum), false);
                        logdat.OpenLogfile();
                        logdat.WriteLog(dt, sitename, Path.GetFileName(kurofilename) + " 分析結果取得成功");
                        logdat.CloseLogTable();
                    }
                    else
                    {
                        LogData.WriteLog(Path.GetFileName(kurofilename) + " 分析結果取得失敗");
                        bRet = false;
                    }
                }
                catch (Exception ex)
                {
                    LogData.WriteLog(Path.GetFileName(kurofilename) + " 分析結果取得失敗");
                    Console.WriteLine(ex.ToString());
                    bRet = false;
                }
            }
            return bRet;
        }
#endif

    }
}
