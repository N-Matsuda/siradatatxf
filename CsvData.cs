﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace SiraDataTransmitter
{
    /// CSVファイルの読み書きを行うクラスです。
    class CsvData
    {
        //指定テーブルをファイルパスで指定されたCSVファイルに書き込みます。ヘッダーの有り無しを指定します。
        public bool WriteCSV(DataTable dt, string filepath, bool wrheader)
        {
            bool ret = true;
            try
            {
                //if( System.IO.File.Exists(filepath) == false )
                //{
                //    System.IO.File.Create(filepath);
                //}
                Encoding enc = Encoding.GetEncoding("Shift_JIS");
                StreamWriter sr = new StreamWriter(filepath, wrheader, enc);
                int colCount = dt.Columns.Count;
                int lastColIndex = colCount - 1;

                //ヘッダを書き込む
                for (int i = 0; i < colCount; i++)
                {
                    //ヘッダの取得
                    string field = dt.Columns[i].Caption;
                    //"で囲む必要があるか調べる
                    //if (field.IndexOf('"') > -1 ||
                    //    field.IndexOf(',') > -1 ||
                    //    field.IndexOf('\r') > -1 ||
                    //   field.IndexOf('\n') > -1 ||
                    //    field.StartsWith(" ") || field.StartsWith("\t") ||
                    //    field.EndsWith(" ") || field.EndsWith("\t"))
                    //{
                    //    if (field.IndexOf('"') > -1)
                    //    {
                    //        //"を""とする
                    //        field = field.Replace("\"", "\"\"");
                    //    }
                    //    field = "\"" + field + "\"";
                    //}
                    //フィールドを書き込む
                    sr.Write(field);
                    //カンマを書き込む
                    if (lastColIndex > i)
                    {
                        sr.Write(',');
                    }
                }
                //改行する
                sr.Write("\r");
                //レコードを書き込む
                foreach (DataRow row in dt.Rows)
                {
                    for (int i = 0; i < colCount; i++)
                    {
                        //フィールドの取得
                        string field = row[i].ToString();
                        //"で囲む必要があるか調べる
                        //if (field.IndexOf('"') > -1 ||
                        //    field.IndexOf(',') > -1 ||
                        //    field.IndexOf('\r') > -1 ||
                        //    field.IndexOf('\n') > -1 ||
                        //    field.StartsWith(" ") || field.StartsWith("\t") ||
                        //    field.EndsWith(" ") || field.EndsWith("\t"))
                        //{
                        //    if (field.IndexOf('"') > -1)
                        //    {
                        //        //"を""とする
                        //        field = field.Replace("\"", "\"\"");
                        //    }
                        //    field = "\"" + field + "\"";
                        //}
                        //フィールドを書き込む
                        sr.Write(field);
                        //カンマを書き込む
                        if (lastColIndex > i)
                        {
                            sr.Write(',');
                        }
                    }
                    //改行する
                    sr.Write("\r");
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ret = false;
            }
            return ret;
        }

        //ファイルパスで指定されたCSVファイルを指定テーブルに読み込みます。ヘッダーの有り無しを指定します。
        public DataTable ReadCSV(string fileName, char delim = ',', bool is1stRowHeader = false)
        {
            DataTable dt = new DataTable();
            try
            {
                Encoding enc = Encoding.GetEncoding("Shift_JIS");
                StreamReader sr = new StreamReader(fileName, enc);

                // CSVファイルを配列へ読み込み
                string text = sr.ReadToEnd();
                sr.Close();
                char[] delimiterChars = { '\r', '\n' };
                string[] csvRows = text.Split(delimiterChars);

                if (csvRows.GetLength(0) > 0)
                {
                    string[] fields = csvRows[0].Split(delim);

                    // DataTable 列の作成
                    for (int i = 0; i < fields.GetLength(0); i++)
                    {
                        if (is1stRowHeader)
                        {
                            // 先頭行をヘッダーとして使用
                            dt.Columns.Add(fields[i], typeof(string));
                        }
                        else
                        {
                            dt.Columns.Add();
                        }
                    }

                    // CSVデータをDataTableへ読み込む
                    int firstDataRow = is1stRowHeader ? 1 : 0;

                    for (int i = firstDataRow; i < csvRows.GetLength(0); i++)
                    {
                        if (csvRows[i].Length > 1)
                        {
                            fields = csvRows[i].Split(delim);
                            DataRow row = dt.NewRow();
                            row.ItemArray = fields;
                            dt.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return dt;
        }

    }
}
