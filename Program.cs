﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Azure.WebJobs;
using System.Threading;
using System.Net.Http;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SiraDataTransmitter
{
    class Program
    {
        static void Main(string[] args)
        {

            //会社毎にLOB社へデータ転送用のスレッドを立ち上げ起動する。
            SiraCompany sircmp = new SiraCompany();
            sircmp.OpenTable("*");
            int iCmpNumTotal = sircmp.GetNumOfRecord();
            List<ComExecClass> ThreadPool = new List<ComExecClass>(); //スレッドプール
            int iCmpNo;


#if false  //for debug
            iCmpNo = 8;
            ComExecClass GComExec = new ComExecClass(iCmpNo);     //通信クラス
            ThreadPool.Add(GComExec);                       //スレッドプールに作成した通信クラス登録
            GComExec.IsWorking = true;

            Thread ComThread = new Thread((new ThreadStart(GComExec.ComThread_Main)));
            ComThread.IsBackground = true;
            ComThread.Start();                              //スレッド実行開始
#else
            for (iCmpNo = 0; iCmpNo < iCmpNumTotal; iCmpNo++)
            {
                ComExecClass GComExec = new ComExecClass(iCmpNo);     //通信クラス
                ThreadPool.Add(GComExec);                       //スレッドプールに作成した通信クラス登録
                GComExec.IsWorking = true;

                Thread ComThread = new Thread((new ThreadStart(GComExec.ComThread_Main)));
                ComThread.IsBackground = true;
                ComThread.Start();                              //スレッド実行開始
            }
#endif
            //各会社毎のデータ転送が終了する(スレッドが全て終了する)のを待ち合わせる。
            bool IsWorking = true;
            while (IsWorking)                                   //全てのスレッドが終了するまで待ち合わせる
            {
                IsWorking = false;
                foreach (ComExecClass cec in ThreadPool)        //各スレッドプールの動作フラグをチェック
                {
                    if (cec.IsWorking == true)
                        IsWorking = true;
                }
                Thread.Sleep(1000);
            }
            SiraData.DeleteOldData();              //古いSIRAデータ削除
            SiraHistory.DeleteOldSiraHistory();    //SIRA旧履歴の削除

        }



    }
}
