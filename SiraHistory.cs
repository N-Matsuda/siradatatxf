﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiraDataTransmitter
{
    class SiraHistory
    {
        //在庫の保存
        public static bool SaveSiraHistory(string skkcode, string datestr,  List<string> tnolist, List<string> invlist, List<string> salelist, List<string> delivlist)
        {
            try
            {
                //各タンク在庫量をDBに登録
                DateTime dt = DateTime.Now.ToLocalTime();
                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;
                string sqlstr = "INSERT INTO SiraHistory (ID, SKKコード,　日付";
                for (int i = 0; i < tnolist.Count; i++)
                {
                    sqlstr += ",タンク" + tnolist[i] + "在庫量, タンク" + tnolist[i] + "販売量, タンク" + tnolist[i] + "荷卸量";
                }
                sqlstr = sqlstr + ") VALUES ('" + count + "','" + skkcode + "','" + datestr + "'";
                for (int i = 0; i < tnolist.Count; i++)
                {
                    sqlstr = sqlstr + ",'" + invlist[i] + "','" + salelist[i] + "','" + delivlist[i] + "'";
                }
                sqlstr = sqlstr + ")";
                DBCtrl.ExecNonQuery(sqlstr);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("在庫履歴書き込み失敗");
                return false;
            }
        }

        //古い在庫履歴の削除
        public static void DeleteOldSiraHistory()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            int hour = dt.Hour;
            //if (hour < 23)  //23時以降に実施
            //    return;

            try
            {
                dt = dt.AddMonths(-3);  //3月前より古い在庫履歴を削除
                string sqlstr = "DELETE FROM SiraHistory WHERE 日付 <= '" + dt.ToString("yyyyMMdd") + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Sira履歴削除失敗");
            }
        }

    }
}
