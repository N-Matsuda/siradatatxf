﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SiraDataTransmitter
{
    class GlobalVar
    {
        public static string DBCONNECTION = "Data Source = skkatgsdb.database.windows.net;Initial Catalog = SkkAtgsDB;User ID=skkkaiadmin;Password = skkkaidb-201806";
    }

    public class DataTableCtrl
    {
        static public void InitializeTable(DataTable dt)
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
                dt = null;
            }
        }

        static public void InitializeDataSet(DataSet ds)
        {
            if (ds != null)
            {
                ds.Clear();
                ds.Tables.Clear();
                ds.Dispose();
                ds = null;
            }
        }
    }
    public class DBCtrl
    {
        static public void ExecSelectAndFillTable(string sqlstr, DataTable dt)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
            dAdp.Fill(dt);
            cn.Close();
        }
        static public void ExecNonQuery(string sqlstr)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            System.Data.SqlClient.SqlCommand Com;
            Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            Com.ExecuteNonQuery();
            cn.Close();
        }
    }

}
