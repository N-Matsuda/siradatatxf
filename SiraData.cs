﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;

namespace SiraDataTransmitter
{
    //SIRA用のデータを管理するクラス
    class SiraData
    {
        private DataTable SIRADataTableDT;

        private const int IdColNo = 0;
        private const int SKKCodeColNo = 1;     //SKKCode
        private const int CurrentDateNo = 2;   //最新登録日
        private const int SIRADataColNo = 3;

        public SiraData()
        {
            DataTableCtrl.InitializeTable(SIRADataTableDT);
        }

        //テーブル読み込み SKKコード
        public void OpenTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    sqlstr = "SELECT * FROM SIRAData WHERE SKKコード= '" + skkcode + "' ORDER BY 日付";
                }
                else
                {
                    sqlstr = "SELECT * FROM SIRAData WHERE SKKコード LIKE '" + skkcode + "%' ORDER BY 日付";
                }
                DataTableCtrl.InitializeTable(SIRADataTableDT);
                SIRADataTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SIRADataTableDT);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return SIRADataTableDT.Rows.Count;
        }

        //SIRAレコード取り出し
        public string GetSIRAData(int lineno)
        {
            try
            {
                if ((SIRADataTableDT != null) && (lineno < SIRADataTableDT.Rows.Count))
                {
                    string data= SIRADataTableDT.Rows[lineno][SIRADataColNo].ToString().TrimEnd();
                    //古いバージョンからあがってきたデータにはSKKコードが含まれる場合があるので削除する。
                    //具体的には相光石油のモバイルルーター(MI)からあがってきたデータの対応
                    int idx = data.IndexOf("M");
                    int idx2 = data.IndexOf("A");
                    if (idx >= 0)
                    {
                        data = data.Substring(idx + 10);
                        SIRADataTableDT.Rows[lineno][SIRADataColNo] = data;
                    }
                    else if ((idx2 >= 0) && (idx2 < 8)) //大宰府インターの場合の対策
                    {
                        data = data.Substring(12);
                        SIRADataTableDT.Rows[lineno][SIRADataColNo] = data;
                    }
                    return data;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //DBテーブル削除 SKKコード
        public void DeleteTableSkkcode(string skkcode)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    sqlstr = "DELETE FROM SIRAData WHERE SKKコード= '" + skkcode + "'";
                }
                else
                {
                    sqlstr = "DELETE FROM SIRAData WHERE SKKコード LIKE '" + skkcode + "%'";
                }
                System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //DBテーブル削除 古いデータ20日前までに変更
        public static void DeleteOldData()
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                try
                {
                    dt = dt.AddHours(-480); //テスト 20日前
                    string sqlstr = "DELETE FROM SIRADATA WHERE 日付 < '" + dt.ToString("yyyyMMddHHmm") + "'";
                    DBCtrl.ExecNonQuery(sqlstr);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //新規SIRA用データ登録
        public void RegisterNewSiraData(string skkcode, string data)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            string dtstr = dt.ToString("yyyyMMddHHmm");
            try
            {
                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;
                string sqlstr = "INSERT INTO  SIRAData (ID,SKKコード,日付,データ) VALUES ('" + count + "','" + skkcode + "','" + dtstr + "','" + data + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


        }
    }
}
