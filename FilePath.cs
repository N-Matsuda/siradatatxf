﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace SiraDataTransmitter
{
    class FilePath
    {
        private string SirLogDataPath; //ログデータへのパス
        private string KuroCsvPath; //黒本CSVファイルのパス
        //private string stLogFilePath;
        private string KuroDataPath;
        //private string stDataPath = "App_Data";
        private string exename = "SiraDataTransmitter.exe";
        private string PdfFormPath;
        private string HdXmlPath;

        public FilePath(string KuroFolderName)
        {
            Assembly myAssembly = Assembly.GetEntryAssembly();
            string ApplicationPath = myAssembly.Location;
            ApplicationPath = ApplicationPath.Substring(0, ApplicationPath.Length - exename.Length);
            //KuroDataPath = ApplicationPath + "App_Data/" + KuroFolderName + "/Kurohon";
            //KuroCsvPath = ApplicationPath + "App_Data/" + KuroFolderName + "/Kurohon/Csv";
            //SirLogDataPath = KuroDataPath + "/Log";
            //PdfFormPath = ApplicationPath + "App_Data/KRH.rse";
            //HdXmlPath = ApplicationPath + "App_Data/" + KuroFolderName + "/Kurohon/Csv";
            KuroDataPath = KuroCsvPath = SirLogDataPath = PdfFormPath = HdXmlPath = ApplicationPath + "App_Data";
            //KuroDataPath = KuroCsvPath = SirLogDataPath = PdfFormPath = HdXmlPath = ApplicationPath;
            try
            {
                if (!Directory.Exists(KuroDataPath))
                {
                    Directory.CreateDirectory(KuroDataPath);
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine("failed to create directory " + KuroDataPath);
            }
        }

        public string GetSirLogDataPath()
        {
            return SirLogDataPath;
        }

        public void SetSirLogDataPath(string LPath)
        {
            SirLogDataPath = LPath;
        }

        public string GetTodayLogDataPath(int lno, bool hdsira)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            if (hdsira == true)
                return SirLogDataPath + "\\LGHD" + dt.ToString("yyMM") + ".csv";
            else
                return SirLogDataPath + "\\LG" + dt.ToString("yyMM") + ".csv";
        }

        //指定の日のLog履歴ファイル名の取り出し
        public string GetDayLogDataPath(DateTime dt, int lno, bool hdsira)
        {
            if (hdsira == true)
                return SirLogDataPath + "\\LGHD" + dt.ToString("yyMM") + ".csv";
            else
                return SirLogDataPath + "\\LG" + dt.ToString("yyMM") + ".csv";
        }

        //黒本パスの取り出し
        public string GetKuroDataPath()
        {
            return KuroDataPath;
        }
        //現在の黒本CSVファイルのパス
        public string GetTodayCsvDataPath(string cmpcode, string sitecode, string tankno)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return KuroCsvPath + "\\" + cmpcode + sitecode + tankno + dt.ToString("yyMM") + ".csv";
        }
        //指定月の黒本CSVファイルのパス
        public string GetDayCsvDataPath(string cmpcode, string sitecode, string tankno, DateTime dt)
        {
            return KuroCsvPath + "\\" + cmpcode + sitecode + tankno + dt.ToString("yyMM") + ".csv";
        }
        //高精度SIRAデータのパス
        public string GetHDSiraDataPath()
        {
            return KuroDataPath + "\\HDSIRA";
        }

        //指定のSIRのファイルのパス
        public string GetSirCSVFilePath(string compcode, string sitecode, DateTime dtn)
        {
            return SirLogDataPath + "\\SIRA_" + compcode + "_" + sitecode + "_" + dtn.ToString("yyyyMMdd_HHmm") + ".csv";
        }

        //指定のSIR(温度データ)のファイルのパス
        public string GetSirTempCSVFilePath(string compcode, DateTime dtn)
        {
            return SirLogDataPath + "\\SIRA_TEMP_" + compcode + "_" + dtn.ToString("yyyyMMdd_HHmm") + ".csv";
        }

        //指定の高精度HIRAのファイルのパス
        public string GetXMLDataPath(string cmpcode, string filedatestr)
        {
            return KuroDataPath + "\\HDSIRA\\HD_SIRA_" + cmpcode + "_" + filedatestr;
        }

        //現在の黒本PDFフォームファイルのパス
        public string GetPdfFormPath()
        {
            return PdfFormPath;
        }

        //現在の黒本PDFファイルのパス
        public string GetTodayPdfDataPath(string cmpcode, string sitecode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return KuroCsvPath + "\\" + cmpcode + sitecode + dt.ToString("yyMM") + ".pdf";
        }
        //先月の黒本PDFファイルのパス
        public string GetLastMonthPdfDataPath(string cmpcode, string sitecode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            dt = dt.AddMonths(-1);
            return KuroCsvPath + "\\" + cmpcode + sitecode + dt.ToString("yyMM") + ".pdf";
        }
        //現在の黒本Excelファイルのパス
        public string GetTodayXlsDataPath(string cmpcode, string sitecode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return KuroCsvPath + "\\" + cmpcode + sitecode + dt.ToString("yyMM") + ".xlsx";
        }

    }
}
