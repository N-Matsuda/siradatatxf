﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace SiraDataTransmitter
{
    //黒本形式のcsvデータを作成、管理するクラス
    class KuroCsvData
    {
        private DataTable KuroDataTable; //通信データから新規作成した、またはCSVファイルから読み込んだ在庫データ

        //日付,開始時在庫,荷卸量,販売量,計算在庫量,在庫量,増減,販売量累計,増減量累計,累計増減率
        private const string stDate = "日付";
        private const string stIniInv = "開始時在庫";
        private const string stDeliv = "荷卸量";
        private const string stSales = "販売量";
        private const string stCalInv = "計算在庫量";
        private const string stInv = "在庫量";
        private const string stDif = "増減";
        private const string stAccSales = "販売量累計";
        private const string stAccDif = "増減量累計";
        private const string stDifPer = "累計増減率";
        private const int COLDATE = 0; //"日付";
        private const int COLINIINV = 1; //"開始時在庫";
        private const int COLDELIV = 2; //"荷卸量";
        private const int COLSALES = 3; //"販売量";
        private const int COLCALINV = 4; //"計算在庫量";
        private const int COLINV = 5; //"在庫量";
        private const int COLDIF = 6; //"増減";
        private const int COLACCSALES = 7; //"販売量累計";
        private const int COLACCDIF = 8; //"増減量累計";
        private const int COLDIFPER = 9; //"累計増減率";
        private string KuroFolderName;
        private string cmpcode;     //会社コード
        private string sitecode;    //施設コード

        //コンストラクタ
        public KuroCsvData(string KuroFName, string cmpc, string sitec)
        {
            DataTableCtrl.InitializeTable(KuroDataTable);
            KuroFolderName = KuroFName;
            cmpcode = cmpc;
            sitecode = sitec;
        }

        //黒本ファイルを開く
        public bool OpenKurofile(string tankno, DateTime dt)
        {
            bool bret = true;
            if (KuroDataTable != null)
                return true;
            try
            {
                FilePath fp = new FilePath(KuroFolderName);
                string dflname = fp.GetDayCsvDataPath(cmpcode, sitecode, tankno, dt);
                if (System.IO.File.Exists(dflname) == true)
                {
                    DataTableCtrl.InitializeTable(KuroDataTable);
                    CsvData cd = new CsvData();
                    KuroDataTable = cd.ReadCSV(dflname, ',', true);
                    if (KuroDataTable.Rows.Count == 0) //読み出したファイルが空の場合バックアップから読み出す
                    {
                        dflname = dflname + ".bak";
                        if (File.Exists(dflname))
                        {
                            DataTableCtrl.InitializeTable(KuroDataTable);
                            KuroDataTable = cd.ReadCSV(dflname, ',', true);
                        }
                    }
                }
                else
                {
                    dflname = dflname + ".bak";
                    CsvData cd = new CsvData();
                    if (File.Exists(dflname))
                    {                       //バックアップファイルがある場合バックアップファイルから読み出す
                        DataTableCtrl.InitializeTable(KuroDataTable);
                        KuroDataTable = cd.ReadCSV(dflname, ',', true);
                    }
                    else
                    {                       //バックアップファイルもない場合、新規作成
                        CreateKuroDataTable();
                        cd.WriteCSV(KuroDataTable, dflname, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                bret = false;
            }
            return bret;
        }

        //ログデータをファイルに保存する
        public void CloseKuroFile(string tankno, DateTime dt)
        {
            if (KuroDataTable == null)
                return;
            try
            {
                CsvData cd = new CsvData();
                FilePath fp = new FilePath(KuroFolderName);
                string dflname = fp.GetDayCsvDataPath(cmpcode, sitecode, tankno, dt);
                cd.WriteCSV(KuroDataTable, dflname, false);
                //バックアップ作成する
                string dflname2 = dflname + ".bak";
                File.Copy(dflname, dflname2, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //黒本用データテーブルを作成
        private void CreateKuroDataTable()
        {
            DataTableCtrl.InitializeTable(KuroDataTable);
            KuroDataTable = new DataTable();
            //レコードのフィールドを定義する
            string[] columnlst = { stDate, stIniInv, stDeliv, stSales, stCalInv, stIniInv, stDif, stAccSales, stAccDif, stDifPer };
            KuroDataTable.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());
        }

        //黒本データテーブルに追加
        public void AddKuroDataTable(DateTime dt, int salesval, int delivval, int invval, int lastinv)
        {
            try
            {
                int prvln = KuroDataTable.Rows.Count;
                int dayln = dt.Day;
                int stinv, calcv, difv, salestotal, diftotal;
                double dvl;
                if (dayln <= prvln) //既に記録がある場合
                {
                    int i = dayln - 1;
                    KuroDataTable.Rows[i][COLDATE] = dt.ToString("d日");      //日付
                    if (dayln == 1) //月の初日
                    {
                        stinv = lastinv;
                    }
                    else
                    {
                        stinv = int.Parse(KuroDataTable.Rows[i - 1][COLINV].ToString().TrimEnd()); //前日の在庫量
                    }
                    KuroDataTable.Rows[i][COLINIINV] = stinv.ToString();    //開始時実在庫
                    KuroDataTable.Rows[i][COLDELIV] = delivval.ToString();    //荷卸し量
                    KuroDataTable.Rows[i][COLSALES] = salesval.ToString();    //販売量
                    calcv = stinv + delivval - salesval;
                    KuroDataTable.Rows[i][COLCALINV] = calcv.ToString(); //計算在庫量
                    KuroDataTable.Rows[i][COLINV] = invval.ToString();    //実在庫量
                    difv = invval - calcv;
                    KuroDataTable.Rows[i][COLDIF] = difv.ToString(); //本日の増減
                    if (dt.Day == 1) //月の初日
                    {
                        diftotal = difv;
                        salestotal = salesval;
                    }
                    else
                    {
                        salestotal = int.Parse(KuroDataTable.Rows[i - 1][COLACCSALES].ToString().TrimEnd());
                        salestotal += salesval;
                        diftotal = int.Parse(KuroDataTable.Rows[i - 1][stAccDif].ToString().TrimEnd());
                        diftotal += difv;
                    }
                    KuroDataTable.Rows[i][COLACCSALES] = salestotal.ToString();  //販売量累計
                    KuroDataTable.Rows[i][COLACCDIF] = diftotal.ToString(); //増減量の累計
                    dvl = (double)diftotal / (double)salestotal;
                    KuroDataTable.Rows[i][COLDIFPER] = dvl.ToString("00.000");
                }
                else
                {
                    DataRow dEditRow = KuroDataTable.NewRow();
                    dEditRow[stDate] = dt.ToString("d日");      //日付
                    dEditRow[stDeliv] = delivval.ToString();    //荷卸し量
                    dEditRow[stSales] = salesval.ToString();    //販売量
                    dEditRow[stInv] = invval.ToString();    //実在庫量
                    if (dayln == 1) //月の初日
                    {
                        dEditRow[stIniInv] = lastinv.ToString();    //開始時実在庫
                        calcv = lastinv + delivval - salesval;
                        dEditRow[stCalInv] = calcv.ToString(); //計算在庫量
                        salestotal = salesval;
                        difv = invval - calcv;
                        diftotal = difv;
                    }
                    else
                    {
                        if (prvln > 0)
                        {
                            prvln--;
                            stinv = int.Parse(KuroDataTable.Rows[prvln][COLINV].ToString().TrimEnd()); //前日の在庫量
                            dEditRow[stIniInv] = KuroDataTable.Rows[prvln][COLINV];    //開始時実在庫
                            calcv = stinv + delivval - salesval;
                            dEditRow[stCalInv] = calcv.ToString(); //計算在庫量
                            salestotal = int.Parse(KuroDataTable.Rows[prvln][COLACCSALES].ToString().TrimEnd());
                            salestotal += salesval;
                            diftotal = int.Parse(KuroDataTable.Rows[prvln][COLACCDIF].ToString().TrimEnd());
                            difv = invval - calcv;
                            diftotal += difv;
                        }
                        else
                        {                                               //月の途中から開始する場合
                            dEditRow[stIniInv] = lastinv.ToString();    //開始時実在庫
                            calcv = lastinv + delivval - salesval;
                            dEditRow[stCalInv] = calcv.ToString(); //計算在庫量
                            salestotal = salesval;
                            difv = invval - calcv;
                            diftotal = difv;
                        }
                    }
                    dEditRow[stAccSales] = salestotal.ToString();  //販売量累計
                    dEditRow[stAccDif] = diftotal.ToString(); //増減量の累計
                    dEditRow[stDif] = difv.ToString(); //本日の増減
                    dvl = (double)diftotal / (double)salestotal;
                    dEditRow[stDifPer] = dvl.ToString("00.000"); //累計増減率
                    KuroDataTable.Rows.Add(dEditRow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
