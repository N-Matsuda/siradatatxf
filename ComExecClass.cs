﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SiraDataTransmitter
{
    //各会社ごとにデバイスからアップロードされたSIRAデータをLOB指定のフォーマットに変更し、LOBへ転送する
    class ComExecClass
    {
        public bool IsWorking { get; set; }
        private int CmpNo;   //会社番号
        private SiraCompany sircmp;
        private SiraSite sirsite;
        private SIRAFunc sirfunc;

        public ComExecClass(int cno)
        {
            IsWorking = true;
            CmpNo = cno;
        }

        //通信スレッドメインルーチン
        public void ComThread_Main()
        {
            sircmp = new SiraCompany(); //SIRA用各会社情報
            sircmp.OpenTable("*");
            sirfunc = new SIRAFunc();
            int iCmpNum = sircmp.GetNumOfRecord();
            string companycode = sircmp.GetCompanyCode(CmpNo);  //SIRA転送用会社コード
            int iSiteNum = 0;
            DateTime curdt, newcurdt, fdate;
            string skkcode;
            bool bRet = false;

            sirsite = new SiraSite();   //SIRA用各施設情報
            sirsite.OpenTable(companycode); //指定会社名で施設情報オープン

            iSiteNum = sirsite.GetNumOfRecord();    //施設数

            Console.WriteLine(sircmp.GetCompanyName(CmpNo) + " start checking");
            int iStMnth = 1;
            SiraData srdata = new SiraData();
            string datstr = "";
            string daystr = "";
            int numrecord = 0;

            for (int i = 0; i < iSiteNum; i++)
            {
                curdt = sirsite.GetCurrentDate(i);
                newcurdt = curdt;
                iStMnth = curdt.Month;

                try
                {
                    skkcode = sirsite.GetSKKCode(i);
                    sirfunc.OpenTable(skkcode);

                    if (true == sirsite.GetHDSira(i))   //高精度SIRの場合はスキップ
                    {
                        //AnalyzeHDSiraSite(CmpNo, i);
                        continue;
                    }
                    int j = 1;
                    int k;
                    srdata.OpenTableSkkcode(skkcode);
                    numrecord = srdata.GetNumOfRecord();

                    for (k = 0; k < numrecord; k++)  //各データを一つづつ処理する。
                    {
                        datstr = srdata.GetSIRAData(k);
                        daystr = datstr.Substring(0, 8).Insert(6, "/").Insert(4, "/") + " 00:00";
                        fdate = DateTime.Parse(daystr); //'日付のストリングをDateTime型へ変換
                        if (fdate.CompareTo(curdt) > 0)  //DBに登録している日付より新しいものを処理する。
                        {
                            Console.WriteLine(skkcode + " start processing");
                            SiraDataTrf siratrf = new SiraDataTrf(companycode, CmpNo, i, j);
                            bRet = siratrf.SirDataAnalysis(datstr);  //黒本ファイルを解析してCSVファイルに落とす処理
                            //転送処理
                            if (bRet == true)
                            {
                                if (sirfunc.GetLOBTrf() == true)               //LOBへデータ転送するか？
                                    bRet = siratrf.SiraDataTrfExec(false);      //CSVファイルをLOBサーバーへ転送処理//20181106 test
                            }
                            if ((bRet == true) && (fdate > newcurdt))
                                newcurdt = fdate;

                        } else
                        {
                            Console.WriteLine(skkcode + daystr + " not processing");
                        }
                        j++;
                    }
                    if (newcurdt > curdt)
                    {
                        sirsite.SetCurrentDate(i, newcurdt);
                        Console.WriteLine("update date " + skkcode); 
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    continue;
                }
            }
            sirsite.UpdateDBDate();    //DBの更新日付を更新する。
            LogData.DeleteOldLog();
            IsWorking = false;
        }
    }
}
