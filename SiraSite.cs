﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;

namespace SiraDataTransmitter
{
    //SIRA分析を行うSSを管理するクラス
    class SiraSite
    {
        private const int MAXTANKNO = 20;       //最大タンク数
        private DataTable SIRASiteTableDT;
        private const int IdColNo = 0;
        private const int SiteNameColNo = 1;    //施設名)
        private const int CompCodeColNo = 2;    //CompanyCode
        private const int SiteCodeColNo = 3;    //SiteCode
        private const int SKKCodeColNo = 4;     //SKKCode
        private const int CurrentDateNo = 5;   //最新登録日
        private const int HDSira = 6;           //0-- 通常のSIRA 1--高精度SIRA

        private const int Tank1TypeColNo = 7;   //Tank1液種
        private const int Tank1LinkColNo = 8;   //Tank1連結
        private const int Tank1CapaColNo = 9;   //Tank1容量
        private const int Tank1LMIvColNo = 10;  //Tank1先月残量
        private const int Tank2TypeColNo = 11;   //Tank2液種
        private const int Tank2LinkColNo = 12;   //Tank2連結
        private const int Tank2CapaColNo = 13;   //Tank2容量
        private const int Tank2LMIvColNo = 14;   //Tank2先月残量
        private const int Tank3TypeColNo = 15;   //Tank3液種
        private const int Tank3LinkColNo = 16;   //Tank3連結
        private const int Tank3CapaColNo = 17;   //Tank3容量
        private const int Tank3LMIvColNo = 18;   //Tank3先月残量
        private const int Tank4TypeColNo = 19;   //Tank4液種
        private const int Tank4LinkColNo = 20;   //Tank4連結
        private const int Tank4CapaColNo = 21;   //Tank4容量
        private const int Tank4LMIvColNo = 22;   //Tank4先月残量
        private const int Tank5TypeColNo = 23;   //Tank5液種
        private const int Tank5LinkColNo = 24;   //Tank5連結
        private const int Tank5CapaColNo = 25;   //Tank4容量
        private const int Tank5LMIvColNo = 26;   //Tank4先月残量
        private const int Tank6TypeColNo = 27;   //Tank6液種
        private const int Tank6LinkColNo = 28;   //Tank6連結
        private const int Tank6CapaColNo = 29;   //Tank6容量
        private const int Tank6LMIvColNo = 30;   //Tank6先月残量
        private const int Tank7TypeColNo = 31;   //Tank7液種
        private const int Tank7LinkColNo = 32;   //Tank7連結
        private const int Tank7CapaColNo = 33;   //Tank7容量
        private const int Tank7LMIvColNo = 34;   //Tank7先月残量
        private const int Tank8TypeColNo = 35;   //Tank8液種
        private const int Tank8LinkColNo = 36;   //Tank8連結
        private const int Tank8CapaColNo = 37;   //Tank8容量
        private const int Tank8LMIvColNo = 38;   //Tank8先月残量
        private const int Tank9TypeColNo = 39;   //Tank9液種
        private const int Tank9LinkColNo = 40;   //Tank9連結
        private const int Tank9CapaColNo = 41;   //Tank9容量
        private const int Tank9LMIvColNo = 42;   //Tank9先月残量
        private const int Tank10TypeColNo = 43;  //Tank10液種
        private const int Tank10LinkColNo = 44;  //Tank10連結
        private const int Tank10CapaColNo = 45;   //Tank10容量
        private const int Tank10LMIvColNo = 46;   //Tank10先月残量
        private const int Tank11LMIvColNo = 50;   //Tank11先月残量
        private const int Tank12LMIvColNo = 54;   //Tank12先月残量
        private const int Tank13LMIvColNo = 58;   //Tank13先月残量
        private const int Tank14LMIvColNo = 62;   //Tank14先月残量
        private const int Tank15LMIvColNo = 66;   //Tank15先月残量
        private const int Tank16LMIvColNo = 70;   //Tank16先月残量
        private const int Tank17LMIvColNo = 74;   //Tank17先月残量
        private const int Tank18LMIvColNo = 78;   //Tank18先月残量
        private const int Tank19LMIvColNo = 82;   //Tank19先月残量
        private const int Tank20LMIvColNo = 86;   //Tank20先月残量

        private string cmpnycode;
        //コンストラクター

        public SiraSite()
        {
            DataTableCtrl.InitializeTable(SIRASiteTableDT);
        }

        //テーブル読み込み
        public void OpenTable(string cmpcode)
        {
            try
            {
                cmpnycode = cmpcode;
                string sqlstr;
                if (cmpnycode == "*")
                    sqlstr = "SELECT * FROM SIRASite";
                else
                    sqlstr = "SELECT * FROM SIRASite WHERE CompanyCode= '" + cmpnycode + "'";
                DataTableCtrl.InitializeTable(SIRASiteTableDT);
                SIRASiteTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SIRASiteTableDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //テーブル読み込み with Skkcode
        public void OpenTableWSkkcode(string skkcode)
        {
            try
            {
                string sqlstr;
                if (cmpnycode == "*")
                    sqlstr = "SELECT * FROM SIRASite";
                else
                    sqlstr = "SELECT * FROM SIRASite WHERE SKKCode= '" + skkcode + "'";
                DataTableCtrl.InitializeTable(SIRASiteTableDT);
                SIRASiteTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SIRASiteTableDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //DBコミット SIRデータ送信日
        public void UpdateDBDate()
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                for (int i = 0; i < SIRASiteTableDT.Rows.Count; i++)
                {
                    sqlstr = "UPDATE SIRASite SET CurrentDate= '" + SIRASiteTableDT.Rows[i][CurrentDateNo].ToString() +
                          "' WHERE SKKCode= '" + SIRASiteTableDT.Rows[i][SKKCodeColNo].ToString() + "'";
                    Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                    Com.ExecuteNonQuery();
                }
                cn.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }
        //DBコミット 先月最終データ
        public void UpdateDBLMInv()
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                for (int i = 0; i < SIRASiteTableDT.Rows.Count; i++)
                {
                    sqlstr = "UPDATE SIRASite SET Tank1LMInv= '" + SIRASiteTableDT.Rows[i][Tank1LMIvColNo].ToString() +
                          "' ,Tank2LMInv = '" + SIRASiteTableDT.Rows[i][Tank2LMIvColNo].ToString() +
                          "' ,Tank3LMInv = '" + SIRASiteTableDT.Rows[i][Tank3LMIvColNo].ToString() +
                          "' ,Tank4LMInv = '" + SIRASiteTableDT.Rows[i][Tank4LMIvColNo].ToString() +
                          "' ,Tank5LMInv = '" + SIRASiteTableDT.Rows[i][Tank5LMIvColNo].ToString() +
                          "' ,Tank6LMInv = '" + SIRASiteTableDT.Rows[i][Tank6LMIvColNo].ToString() +
                          "' ,Tank7LMInv = '" + SIRASiteTableDT.Rows[i][Tank7LMIvColNo].ToString() +
                          "' ,Tank8LMInv = '" + SIRASiteTableDT.Rows[i][Tank8LMIvColNo].ToString() +
                          "' ,Tank9LMInv = '" + SIRASiteTableDT.Rows[i][Tank9LMIvColNo].ToString() +
                          "' ,Tank10LMInv = '" + SIRASiteTableDT.Rows[i][Tank10LMIvColNo].ToString() +
                          "' ,Tank11LMInv = '" + SIRASiteTableDT.Rows[i][Tank11LMIvColNo].ToString() +
                          "' ,Tank12LMInv = '" + SIRASiteTableDT.Rows[i][Tank12LMIvColNo].ToString() +
                          "' ,Tank13LMInv = '" + SIRASiteTableDT.Rows[i][Tank13LMIvColNo].ToString() +
                          "' ,Tank14LMInv = '" + SIRASiteTableDT.Rows[i][Tank14LMIvColNo].ToString() +
                          "' ,Tank15LMInv = '" + SIRASiteTableDT.Rows[i][Tank15LMIvColNo].ToString() +
                          "' ,Tank16LMInv = '" + SIRASiteTableDT.Rows[i][Tank16LMIvColNo].ToString() +
                          "' ,Tank17LMInv = '" + SIRASiteTableDT.Rows[i][Tank17LMIvColNo].ToString() +
                          "' ,Tank18LMInv = '" + SIRASiteTableDT.Rows[i][Tank18LMIvColNo].ToString() +
                          "' ,Tank19LMInv = '" + SIRASiteTableDT.Rows[i][Tank19LMIvColNo].ToString() +
                          "' ,Tank20LMInv = '" + SIRASiteTableDT.Rows[i][Tank20LMIvColNo].ToString() +
                          "' WHERE SKKCode= '" + SIRASiteTableDT.Rows[i][SKKCodeColNo].ToString() + "'";
                    Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                    Com.ExecuteNonQuery();
                }
                cn.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return SIRASiteTableDT.Rows.Count;
        }

        //施設名一覧取得
        public List<string> GetSiteNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < SIRASiteTableDT.Rows.Count; i++)
            {
                list.Add(SIRASiteTableDT.Rows[i][SiteNameColNo].ToString().TrimEnd());
            }
            return list;
        }

        //行番号から施設名取得
        public string GetSiteName(int lineno)
        {
            string sitename = "";
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count))
            {
                return SIRASiteTableDT.Rows[lineno][SiteNameColNo].ToString().TrimEnd();
            }
            return sitename;
        }

        //行番号からカンパニーコード取得
        public string GetCompanyCode(int lineno)
        {
            string cpname = "";
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count))
            {
                return SIRASiteTableDT.Rows[lineno][CompCodeColNo].ToString().TrimEnd();
            }
            return cpname;
        }

        //行番号から施設コード取得
        public string GetSiteCode(int lineno)
        {
            string cpname = "";
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count))
            {
                return SIRASiteTableDT.Rows[lineno][SiteCodeColNo].ToString().TrimEnd();
            }
            return cpname;
        }

        //行番号からSKKコード取得
        public string GetSKKCode(int lineno)
        {
            string cpname = "";
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count))
            {
                return SIRASiteTableDT.Rows[lineno][SKKCodeColNo].ToString().TrimEnd();
            }
            return cpname;
        }

        //最新登録日取得
        public DateTime GetCurrentDate(int lineno)
        {
            DateTime dt = new DateTime(2014, 12, 1);
            string dstr;
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count))
            {
                try
                {
                    dstr = SIRASiteTableDT.Rows[lineno][CurrentDateNo].ToString().TrimEnd();
                    dstr = dstr.Insert(6, "/");
                    dstr = dstr.Insert(4, "/");
                    dstr = dstr + " 00:00";
                    dt = DateTime.Parse(dstr); //'日付のストリングをDateTime型へ変換
                    return dt;
                }
                catch
                {
                    return dt;
                }
            }
            return dt;
        }

        //最新登録日設定
        public void SetCurrentDate(int lineno, DateTime newdt)
        {
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count))
            {
                try
                {
                    string dstr = newdt.ToString("yyyyMMdd");
                    SIRASiteTableDT.Rows[lineno][CurrentDateNo] = dstr;
                }
                catch
                {
                    ;
                }
            }
        }

        //行番号とタンク番号(1-20)から液種取得
        public string GetTankOilType(int lineno, int tankno)
        {
            string cpname = "";
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count) && (tankno > 0) && (tankno <= MAXTANKNO))
            {
                try
                {
                    return SIRASiteTableDT.Rows[lineno][Tank1TypeColNo + 4 * (tankno - 1)].ToString().TrimEnd();
                }
                catch
                {
                    return cpname;
                }
            }
            return cpname;
        }

        //行番号とタンク番号(1-20)から連結情報取得し、連結なしのタンクか連結タンクの先頭ならばtrueを返す
        public bool GetTankNotLinkedOrLinkedFirst(int lineno, int tankno)
        {
            bool blinked = false;
            string lnkstr;
            int lnkno;
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count) && (tankno > 0) && (tankno <= MAXTANKNO))
            {
                try
                {
                    lnkstr = SIRASiteTableDT.Rows[lineno][Tank1TypeColNo + 4 * (tankno - 1) + 1].ToString().TrimEnd();
                    if (lnkstr.Length >= 2)
                        lnkstr = lnkstr.Substring(0, 2);
                    else
                        lnkstr = "0";
                    lnkno = int.Parse(lnkstr);
                    if ((lnkno == 0) || (lnkno == tankno)) //連結なしか、連結の先頭ならtrueを返す
                        blinked = true;
                    return blinked;
                }
                catch
                {
                    return blinked;
                }
            }
            return blinked;
        }
        //タンク連結文字列を返す。たとえば、タンク2と3が連結ならば"Tank2,3"を返す。
        public string GetTankString(int lineno, int fsttank)
        {
            string tankstr = "Tank";
            string lnkstr;
            string tnostr;
            int lnkno;
            int len;
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count) && (fsttank > 0) && (fsttank <= MAXTANKNO))
            {
                try
                {
                    lnkstr = SIRASiteTableDT.Rows[lineno][Tank1TypeColNo + 4 * (fsttank - 1) + 1].ToString().TrimEnd();
                    tnostr = lnkstr.Substring(0, 2);
                    lnkno = int.Parse(tnostr);
                    if ((lnkno == 0) || (lnkno != fsttank)) //連結なしか、連結の先頭でないならタンク番号一つを返す
                    {
                        tankstr = tankstr + fsttank.ToString();
                    }
                    else
                    {                                      //連結の先頭
                        len = lnkstr.Length;
                        tankstr = tankstr + fsttank.ToString();
                        for (int i = 2; i < len; i += 2)
                        {
                            tnostr = lnkstr.Substring(i, 2);
                            lnkno = int.Parse(tnostr);
                            tankstr = tankstr + "," + lnkno.ToString();
                        }
                    }
                    return tankstr;
                }
                catch
                {
                    return tankstr;
                }
            }
            return tankstr;
        }

        //高精度SIRAかどうか true--高精度SIRA(WetstockLive) false--通常SIRA
        public bool GetHDSira(int lineno)
        {
            bool hdsira = false;
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count))
            {
                try
                {
                    int hdnum = (int)SIRASiteTableDT.Rows[lineno][HDSira];
                    hdsira = (hdnum > 0) ? true : false;

                }
                catch
                {
                    return hdsira;
                }
            }
            return hdsira;
        }

        //行番号とタンク番号(1-20)からタンク容量取得
        public int GetTankCapa(int lineno, int tankno)
        {
            int capav = 0;
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count) && (tankno > 0) && (tankno <= MAXTANKNO))
            {
                try
                {
                    return (int)SIRASiteTableDT.Rows[lineno][Tank1TypeColNo + 4 * (tankno - 1) + 2];
                }
                catch
                {
                    return capav;
                }
            }
            return capav;
        }

        //行番号とタンク番号(1-20)から先月末の在庫量返す
        public int GetTankLMInv(int lineno, int tankno)
        {
            int invv = 0;
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count) && (tankno > 0) && (tankno <= MAXTANKNO))
            {
                try
                {
                    return (int)SIRASiteTableDT.Rows[lineno][Tank1TypeColNo + 4 * (tankno - 1) + 3];
                }
                catch (Exception ex)
                {
                    return invv;
                }
            }
            return invv;
        }

        //行番号とタンク番号(1-20)から月末の在庫量設定
        public void SetTankLMInv(int lineno, int tankno, int invval)
        {
            if ((SIRASiteTableDT != null) && (lineno < SIRASiteTableDT.Rows.Count) && (tankno > 0) && (tankno <= MAXTANKNO))
            {
                try
                {
                    SIRASiteTableDT.Rows[lineno][Tank1TypeColNo + 4 * (tankno - 1) + 3] = invval;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

    }
}
